from gluon.storage import Storage


settings = Storage()

settings.migrate = True
settings.title = 'Trilhas Poeticas'
settings.subtitle = 'powered by web2py'
settings.author = 'Coletivo Palavra'
settings.author_email = 'palavracoletivo@gmail.com'
settings.keywords = ''
settings.description = ''
settings.layout_theme = 'Site'
settings.database_uri = 'mysql://root:dc76e9f0c0006e8f919e0c515c66dbba3982f785@localhost/trilhas02'
settings.dbuser = 'root'
settings.dbpasswd = 'dc76e9f0c0006e8f919e0c515c66dbba3982f785'
settings.security_key = 'b998d97f-b705-47f3-8804-157c15b50b0f'
settings.email_server = 'localhost'
settings.email_sender = 'palavracoletivo@gmail.com'
settings.email_login = ''
settings.login_method = 'local'
settings.login_config = ''
settings.plugins = []