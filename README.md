Trilhas Poéticas
=============

Dependências
------------

Este projeto é baseado no framework do Web2Py (www.web2py.com) e DB MySQL (www.mysql.com).

0. Baixar e instalar o codigo fonte do web2py localmente (http://www.web2py.com/init/default/download)
0. Baixar e instalar o mysql-server 
0. Configurar o usuario root e senha no mysql
0. Instalar o GIT

Instalação
-----------
0. Acessar a pasta web2py/applications
0. Clonar o repositorio `git clone https://USUARIO@bitbucket.org/barretto/trilhaspoeticas.git`
0. Criar o seu branch a partir do master `git checkout -b NOMEDOBRANCH`
0. Enviar o seu branch de volta ao servidor `git push origin NOMEDOBRANCH`
0. Executar o web2py e acessar https://localhost:8000/admin
0. Acessar o arquivo db.py e 0.py e configurar o login e senha do MYSQL
0. Tentar acessar http://localhost:8000/trilhaspoeticas

Troubleshooting
---------------
* Database nao existe
```
mysql -u root -p
create database trilhas02;
```
* Tabela auth.user não existe
Rodar o script trilhas.sql que está na raiz da pasta trilhaspoeticas

* mysql access denied for user 'root'@'localhost'
```
mysql -u root -p
GRANT ALL PRIVILEGES ON *.* to 'root'@'localhost' IDENTIFIED BY 'suasenhaderoot';
FLUSH PRIVILEGES;
```

FLUXO DE TRABALHO
-----------------

0. Desenvolva localmente.
0. Ao finalizar um checkpoint ou funcionalidade, utilize o `git commit -a -m 'mensagem do commit'` para realizar o commit.
0. Ao finalizar o 'dia de trabalho' ou fechar um conjunto de funcionalidades, dê um push dos commits `git push`
0. Teste as funcionalidades desenvolvidas. Se estiverem ok para irem para o servidor do LATE, faça um Pull Request (https://bitbucket.org/barretto/trilhaspoeticas/pull-requests/new)
0. As funcionalidades desenvolvidas devem ser testadas inicialmente no servidor do LATE.
0. Ao final da semana, será feito um merge entre os branches de desenvolvimento para o master.
0. Depois disso, será posto em produção
