-- MySQL dump 10.13  Distrib 5.5.38, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: trilhas02
-- ------------------------------------------------------
-- Server version	5.5.38-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


use trilhas02;

--
-- Table structure for table `auth_cas`
--

DROP TABLE IF EXISTS `auth_cas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_cas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `service` varchar(512) DEFAULT NULL,
  `ticket` varchar(512) DEFAULT NULL,
  `renew` char(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id__idx` (`user_id`),
  CONSTRAINT `auth_cas_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_cas`
--

LOCK TABLES `auth_cas` WRITE;
/*!40000 ALTER TABLE `auth_cas` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_cas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_event`
--

DROP TABLE IF EXISTS `auth_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time_stamp` datetime DEFAULT NULL,
  `client_ip` varchar(512) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `origin` varchar(512) DEFAULT NULL,
  `description` longtext,
  PRIMARY KEY (`id`),
  KEY `user_id__idx` (`user_id`),
  CONSTRAINT `auth_event_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=161 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_event`
--

LOCK TABLES `auth_event` WRITE;
/*!40000 ALTER TABLE `auth_event` DISABLE KEYS */;
INSERT INTO `auth_event` VALUES (1,'2015-10-15 23:46:20','189.6.27.37',NULL,'auth','Group 1 created'),(2,'2015-10-15 23:46:20','189.6.27.37',1,'auth','User 1 Registered'),(3,'2015-10-16 13:33:49','186.222.191.196',NULL,'auth','Group 2 created'),(4,'2015-10-16 13:33:49','186.222.191.196',2,'auth','User 2 Registered'),(5,'2015-10-16 14:56:50','186.222.191.196',NULL,'auth','Group 3 created'),(6,'2015-10-16 14:56:50','186.222.191.196',3,'auth','User 3 Registered'),(7,'2015-10-16 16:29:45','186.222.191.196',1,'auth','User 1 Logged-in'),(8,'2015-10-16 16:45:57','186.222.191.196',3,'auth','User 3 Logged-in'),(9,'2015-10-16 19:06:33','186.222.191.196',2,'auth','User 2 Logged-in'),(10,'2015-10-16 19:44:02','186.222.191.196',2,'auth','User 2 Logged-in'),(11,'2015-10-16 20:31:09','177.17.211.83',3,'auth','User 3 Logged-in'),(12,'2015-10-16 23:25:17','177.167.255.210',2,'auth','User 2 Logged-in'),(13,'2015-10-17 10:19:27','177.17.211.83',3,'auth','User 3 Logged-in'),(14,'2015-10-21 10:20:04','189.6.27.37',1,'auth','User 1 Logged-in'),(15,'2015-10-21 14:41:09','187.116.87.78',2,'auth','User 2 Logged-in'),(16,'2015-10-21 15:11:47','187.116.87.78',1,'auth','User 1 Logged-in'),(17,'2015-10-28 15:54:16','179.214.19.48',3,'auth','User 3 Logged-in'),(18,'2015-10-28 15:54:39','179.214.19.48',1,'auth','User 1 Logged-in'),(19,'2015-10-28 15:58:11','179.214.19.48',2,'auth','User 2 Logged-in'),(20,'2015-10-28 19:36:38','179.186.109.69',3,'auth','User 3 Logged-in'),(21,'2015-10-31 21:49:06','191.33.149.227',3,'auth','User 3 Logged-in'),(22,'2015-11-06 15:38:59','179.214.19.48',3,'auth','User 3 Logged-in'),(23,'2015-11-06 16:39:05','179.214.19.48',1,'auth','User 1 Logged-in'),(24,'2015-11-11 08:58:09','179.214.19.48',2,'auth','User 2 Logged-in'),(25,'2015-11-11 14:16:24','179.214.19.48',2,'auth','User 2 Logged-in'),(26,'2015-11-11 14:30:32','179.214.19.48',3,'auth','User 3 Logged-in'),(27,'2015-11-11 15:16:14','179.214.19.48',1,'auth','User 1 Logged-in'),(28,'2015-11-11 16:20:09','179.214.19.48',3,'auth','User 3 Logged-in'),(29,'2015-11-11 16:34:09','179.214.19.48',1,'auth','User 1 Logged-in'),(30,'2015-11-11 16:38:09','179.214.19.48',2,'auth','User 2 Logged-in'),(31,'2015-11-17 12:12:47','189.6.27.37',1,'auth','User 1 Logged-in'),(32,'2015-11-17 14:25:49','189.6.27.37',1,'auth','User 1 Logged-out'),(33,'2015-11-17 14:26:53','189.6.27.37',1,'auth','User 1 Logged-in'),(34,'2015-11-18 00:35:45','179.179.55.3',3,'auth','User 3 Logged-in'),(35,'2015-11-18 00:36:24','179.179.55.3',3,'auth','User 3 Profile updated'),(36,'2015-11-18 00:47:34','189.6.27.37',1,'auth','User 1 Logged-in'),(37,'2015-11-18 11:32:11','179.214.19.48',2,'auth','User 2 Logged-in'),(38,'2015-11-18 12:02:17','186.213.218.31',NULL,'auth','Group 4 created'),(39,'2015-11-18 12:02:17','186.213.218.31',4,'auth','User 4 Registered'),(40,'2015-11-18 12:20:35','179.214.19.48',2,'auth','User 2 Logged-in'),(41,'2015-11-18 15:10:16','179.214.19.48',1,'auth','User 1 Logged-in'),(42,'2015-11-18 15:10:50','179.214.19.48',3,'auth','User 3 Logged-in'),(43,'2015-11-18 16:01:46','179.214.19.48',2,'auth','User 2 Logged-in'),(44,'2015-11-20 00:11:15','189.6.25.56',1,'auth','User 1 Logged-in'),(45,'2015-11-20 10:35:51','179.182.160.94',3,'auth','User 3 Logged-in'),(46,'2015-11-20 17:39:29','189.6.25.56',1,'auth','User 1 Logged-in'),(47,'2015-11-20 17:39:50','187.59.142.234',4,'auth','User 4 Logged-in'),(48,'2015-11-23 16:14:09','189.6.25.56',1,'auth','User 1 Logged-in'),(49,'2015-11-24 15:25:25','179.214.19.48',2,'auth','User 2 Logged-in'),(50,'2015-11-24 21:57:17','187.113.17.11',3,'auth','User 3 Logged-in'),(51,'2015-11-25 01:11:31','189.6.25.56',1,'auth','User 1 Logged-in'),(52,'2015-11-25 10:13:09','187.113.17.11',3,'auth','User 3 Logged-in'),(53,'2015-11-25 13:50:23','187.113.17.11',3,'auth','User 3 Logged-in'),(54,'2015-11-25 15:09:02','179.214.19.48',1,'auth','User 1 Logged-in'),(55,'2015-11-25 15:13:27','179.214.19.48',3,'auth','User 3 Logged-in'),(56,'2015-11-25 15:15:17','179.214.19.48',2,'auth','User 2 Logged-in'),(57,'2015-11-26 16:06:55','189.6.25.56',1,'auth','User 1 Logged-in'),(58,'2015-11-27 15:43:38','189.6.25.56',1,'auth','User 1 Logged-in'),(59,'2015-12-02 16:38:36','189.6.26.24',1,'auth','User 1 Logged-in'),(60,'2015-12-02 17:19:12','189.6.26.24',1,'auth','User 1 Logged-out'),(61,'2015-12-02 17:25:25','189.6.26.24',1,'auth','User 1 Logged-in'),(62,'2015-12-03 00:36:56','177.116.3.231',1,'auth','User 1 Logged-in'),(63,'2015-12-03 16:26:30','189.6.26.24',1,'auth','User 1 Logged-in'),(64,'2015-12-03 18:39:13','200.219.132.105',NULL,'auth','Group 7 created'),(65,'2015-12-03 18:39:13','200.219.132.105',5,'auth','User 5 Registered'),(66,'2015-12-04 14:49:40','200.219.133.150',5,'auth','User 5 Logged-in'),(67,'2015-12-07 16:39:38','200.219.133.150',5,'auth','User 5 Logged-in'),(68,'2015-12-07 21:26:39','189.6.25.174',1,'auth','User 1 Logged-in'),(69,'2015-12-08 14:27:06','189.6.25.174',1,'auth','User 1 Logged-in'),(70,'2015-12-10 22:44:56','189.6.25.174',1,'auth','User 1 Logged-in'),(71,'2016-02-07 16:28:37','189.6.26.59',1,'auth','User 1 Logged-in'),(72,'2016-02-29 15:38:38','189.6.26.59',1,'auth','User 1 Logged-in'),(73,'2016-03-08 17:29:36','189.6.26.59',1,'auth','User 1 Logged-in'),(74,'2016-03-18 16:03:00','189.6.24.177',1,'auth','User 1 Logged-in'),(75,'2016-03-18 16:14:41','189.6.24.177',1,'auth','User 1 Logged-out'),(76,'2016-03-22 20:19:18','179.179.48.199',1,'auth','User 1 Logged-in'),(77,'2016-03-22 22:01:14','179.179.48.199',1,'auth','User 1 Logged-in'),(78,'2016-03-22 22:01:45','179.179.48.199',1,'auth','User 1 Logged-in'),(79,'2016-03-24 21:15:22','189.6.24.177',1,'auth','User 1 Logged-in'),(80,'2016-04-01 14:44:32','189.6.102.243',1,'auth','User 1 Logged-in'),(81,'2016-04-01 15:36:53','189.6.102.243',1,'auth','User 1 Logged-out'),(82,'2016-04-09 22:40:15','179.174.24.50',1,'auth','User 1 Logged-in'),(83,'2016-04-26 16:07:18','189.6.27.171',NULL,'auth','User 1 Password reset'),(84,'2016-04-26 16:11:13','189.6.27.171',1,'auth','User 1 Logged-in'),(85,'2016-04-27 00:56:12','189.6.27.171',1,'auth','User 1 Logged-in'),(86,'2016-04-27 02:03:07','189.6.27.171',1,'auth','User 1 Logged-in'),(87,'2016-04-28 14:34:01','189.6.27.171',1,'auth','User 1 Logged-in'),(88,'2016-05-03 15:28:30','189.6.27.171',1,'auth','User 1 Logged-in'),(89,'2016-05-03 17:13:43','189.6.27.171',1,'auth','User 1 Logged-in'),(90,'2016-05-03 21:07:24','177.79.31.66',1,'auth','User 1 Logged-in'),(91,'2016-05-04 16:50:54','189.6.27.171',1,'auth','User 1 Logged-in'),(92,'2016-05-04 16:57:23','189.6.27.171',1,'auth','User 1 Logged-out'),(93,'2016-05-04 19:16:39','189.6.27.171',1,'auth','User 1 Logged-in'),(94,'2016-05-04 20:13:44','189.6.27.171',1,'auth','User 1 Logged-out'),(95,'2016-05-04 22:53:34','189.6.27.171',1,'auth','User 1 Logged-in'),(96,'2016-05-04 22:54:29','189.6.27.171',1,'auth','User 1 Profile updated'),(97,'2016-05-08 02:19:48','201.22.186.95',4,'auth','User 4 Logged-in'),(98,'2016-05-09 23:00:47','177.97.3.133',1,'auth','User 1 Logged-in'),(99,'2016-05-10 21:08:44','187.72.79.250',1,'auth','User 1 Logged-in'),(100,'2016-05-10 23:00:51','187.72.79.250',1,'auth','User 1 Logged-in'),(101,'2016-05-11 15:57:39','189.6.102.243',2,'auth','User 2 Logged-in'),(102,'2016-05-11 15:58:28','189.6.102.243',2,'auth','User 2 Logged-out'),(103,'2016-05-11 15:58:51','189.6.102.243',2,'auth','User 2 Logged-in'),(104,'2016-05-11 15:59:28','189.6.102.243',1,'auth','User 1 Logged-in'),(105,'2016-05-11 16:01:08','189.6.102.243',1,'auth','User 1 Logged-out'),(106,'2016-05-11 16:01:59','189.6.102.243',1,'auth','User 1 Logged-in'),(107,'2016-05-11 16:13:57','189.6.102.243',1,'auth','User 1 Logged-out'),(108,'2016-05-11 16:15:20','189.6.102.243',1,'auth','User 1 Logged-in'),(109,'2016-05-11 18:06:58','189.6.102.243',2,'auth','User 2 Logged-out'),(110,'2016-05-11 18:07:51','189.6.102.243',NULL,'auth','User 2 Password reset'),(111,'2016-05-11 18:08:03','189.6.102.243',2,'auth','User 2 Logged-in'),(112,'2016-05-12 21:41:34','179.181.124.89',1,'auth','User 1 Logged-in'),(113,'2016-05-19 16:52:24','177.13.226.7',NULL,'auth','Group 8 created'),(114,'2016-05-19 16:52:24','177.13.226.7',6,'auth','User 6 Registered'),(115,'2016-06-11 09:25:34','179.183.98.31',3,'auth','User 3 Logged-in'),(116,'2016-06-11 14:35:48','179.183.98.31',3,'auth','User 3 Logged-in'),(117,'2016-06-11 14:39:23','179.183.98.31',3,'auth','User 3 Logged-out'),(118,'2016-06-11 15:16:15','179.183.98.31',3,'auth','User 3 Logged-in'),(119,'2016-06-11 15:17:26','179.183.98.31',3,'auth','User 3 Logged-out'),(120,'2016-06-11 16:05:04','179.183.98.31',3,'auth','User 3 Logged-in'),(121,'2016-06-11 16:05:28','179.183.98.31',3,'auth','User 3 Logged-out'),(122,'2016-06-11 16:36:11','179.183.98.31',3,'auth','User 3 Logged-in'),(123,'2016-06-11 16:39:43','179.183.98.31',3,'auth','User 3 Logged-out'),(124,'2016-06-11 16:44:28','179.183.98.31',3,'auth','User 3 Logged-in'),(125,'2016-06-11 16:44:46','179.183.98.31',3,'auth','User 3 Logged-out'),(126,'2016-06-11 17:05:58','179.183.98.31',3,'auth','User 3 Logged-in'),(127,'2016-06-11 17:34:37','179.183.98.31',3,'auth','User 3 Logged-out'),(128,'2016-06-12 11:32:54','179.183.98.31',3,'auth','User 3 Logged-in'),(129,'2016-06-12 20:34:58','189.6.16.177',3,'auth','User 3 Logged-in'),(130,'2016-06-14 21:29:49','177.43.145.87',1,'auth','User 1 Logged-in'),(131,'2016-06-15 00:28:18','189.6.16.177',3,'auth','User 3 Logged-in'),(132,'2016-06-15 00:41:22','189.6.16.177',3,'auth','User 3 Logged-out'),(133,'2016-06-16 15:55:50','189.6.27.171',1,'auth','User 1 Logged-in'),(134,'2016-06-16 21:33:18','177.18.16.110',3,'auth','User 3 Logged-in'),(135,'2016-06-16 22:21:31','177.18.16.110',3,'auth','User 3 Logged-in'),(136,'2016-06-16 22:31:55','177.18.16.110',3,'auth','User 3 Logged-out'),(137,'2016-06-16 22:32:38','177.18.16.110',3,'auth','User 3 Logged-in'),(138,'2016-06-16 22:37:42','177.18.16.110',3,'auth','User 3 Logged-in'),(139,'2016-06-16 22:41:31','177.18.16.110',3,'auth','User 3 Logged-out'),(140,'2016-06-16 22:42:53','177.18.16.110',3,'auth','User 3 Logged-in'),(141,'2016-06-17 00:01:17','177.18.16.110',3,'auth','User 3 Logged-out'),(142,'2016-06-18 14:44:30','177.43.17.198',3,'auth','User 3 Logged-in'),(143,'2016-06-18 16:19:05','177.43.17.198',3,'auth','User 3 Logged-out'),(144,'2016-06-18 16:24:12','177.43.17.198',3,'auth','User 3 Logged-in'),(145,'2016-06-18 16:26:26','177.43.17.198',3,'auth','User 3 Profile updated'),(146,'2016-06-21 23:29:36','179.186.102.216',3,'auth','User 3 Logged-in'),(147,'2016-06-21 23:38:51','179.186.102.216',3,'auth','User 3 Logged-out'),(148,'2016-06-21 23:41:22','179.186.102.216',3,'auth','User 3 Logged-in'),(149,'2016-06-21 23:47:05','179.186.102.216',3,'auth','User 3 Logged-out'),(150,'2016-07-01 09:59:18','189.6.24.150',1,'auth','User 1 Logged-in'),(151,'2016-07-03 10:29:37','189.6.101.67',2,'auth','User 2 Logged-in'),(152,'2016-07-03 12:17:45','189.6.101.67',2,'auth','User 2 Logged-out'),(153,'2016-07-03 12:18:26','189.6.101.67',NULL,'auth','Group 9 created'),(154,'2016-07-03 12:18:26','189.6.101.67',7,'auth','User 7 Registered'),(155,'2016-07-03 12:19:02','189.6.101.67',7,'auth','User 7 Logged-out'),(156,'2016-07-03 12:19:10','189.6.101.67',2,'auth','User 2 Logged-in'),(157,'2016-07-03 12:19:33','189.6.101.67',2,'auth','User 2 Logged-out'),(158,'2016-07-03 12:19:39','189.6.101.67',7,'auth','User 7 Logged-in'),(159,'2016-07-03 20:00:57','191.249.109.202',3,'auth','User 3 Logged-in'),(160,'2016-07-04 00:13:57','187.114.246.156',4,'auth','User 4 Logged-in');
/*!40000 ALTER TABLE `auth_event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(512) DEFAULT NULL,
  `description` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
INSERT INTO `auth_group` VALUES (1,'user_1','Group uniquely assigned to user 1'),(2,'user_2','Group uniquely assigned to user 2'),(3,'user_3','Group uniquely assigned to user 3'),(4,'user_4','Group uniquely assigned to user 4'),(5,'manager','gestores do sistema'),(6,'admin','administradores do sistema'),(7,'user_5','Group uniquely assigned to user 5'),(8,'user_6','Group uniquely assigned to user 6'),(9,'user_7','Group uniquely assigned to user 7');
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_membership`
--

DROP TABLE IF EXISTS `auth_membership`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_membership` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id__idx` (`user_id`),
  KEY `group_id__idx` (`group_id`),
  CONSTRAINT `auth_membership_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `auth_membership_ibfk_2` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_membership`
--

LOCK TABLES `auth_membership` WRITE;
/*!40000 ALTER TABLE `auth_membership` DISABLE KEYS */;
INSERT INTO `auth_membership` VALUES (1,1,1),(2,2,2),(3,3,3),(4,4,4),(5,4,5),(7,1,5),(8,2,6),(9,3,6),(10,1,6),(11,5,7),(12,1,5),(13,6,8),(14,7,9),(15,7,9);
/*!40000 ALTER TABLE `auth_membership` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) DEFAULT NULL,
  `name` varchar(512) DEFAULT NULL,
  `table_name` varchar(512) DEFAULT NULL,
  `record_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `group_id__idx` (`group_id`),
  CONSTRAINT `auth_permission_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(128) DEFAULT NULL,
  `last_name` varchar(128) DEFAULT NULL,
  `email` varchar(512) DEFAULT NULL,
  `password` varchar(512) DEFAULT NULL,
  `registration_key` varchar(512) DEFAULT NULL,
  `reset_password_key` varchar(512) DEFAULT NULL,
  `registration_id` varchar(512) DEFAULT NULL,
  `avatar` varchar(512) DEFAULT NULL,
  `descricao` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (1,'Andre','Freitas','dedebf@gmail.com','pbkdf2(1000,20,sha512)$a6599c1344f36adf$65d0499fcaf86cd252528d9ba8555a537a7c8a4c','','','','auth_user.Avatar.86b0aa8cbbd8ceef.30325f5472696c6861735f50425f53696d626f6c6f5f62674b2e706e67.png','Descrição de André Freitas'),(2,'Francisco','Barretto','kikobarretto@gmail.com','pbkdf2(1000,20,sha512)$870867bb707aaa1d$97f22ac1d3a125bd2f9d974c0796b8fe8665a75b','','','',NULL,NULL),(3,'Juliana','Hilário','julianahdesousa@gmail.com','pbkdf2(1000,20,sha512)$a18bceba029501e6$895e235ecfb6efdee6246ca3fcb0b0b3770d9852','','','','','TESTE TESTE TESTE'),(4,'João Victor','Pacífico','Joaovictorpd@gmail.com','pbkdf2(1000,20,sha512)$aa70106a351c0924$0357060f7aee80b5e2009c7d4052aa8511736023','','','',NULL,NULL),(5,'thiago','Gualberto','tdelima@gmail.com','pbkdf2(1000,20,sha512)$b2851e091ce3c076$b1347fec23e21977040224146ff2daa9cce3673f','','','',NULL,NULL),(6,'mauricio','chades','maudca@gmail.com','pbkdf2(1000,20,sha512)$991371021e201903$b771e3d2997fcc8fed4cd0887a893b9a80ade444','','','','','hei hou'),(7,'teste','teste','teste@teste.com','pbkdf2(1000,20,sha512)$a1cc3d80d200d597$bc6734a9e7fb2b76ddb769a8a71612e211b64bb3','','','','','teste usuario comum');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_parental_ratings`
--

DROP TABLE IF EXISTS `t_parental_ratings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_parental_ratings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `f_name` varchar(512) NOT NULL,
  `f_description` varchar(512) NOT NULL,
  `is_active` char(1) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by__idx` (`created_by`),
  KEY `modified_by__idx` (`modified_by`),
  CONSTRAINT `t_parental_ratings_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `auth_user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `t_parental_ratings_ibfk_2` FOREIGN KEY (`modified_by`) REFERENCES `auth_user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_parental_ratings`
--

LOCK TABLES `t_parental_ratings` WRITE;
/*!40000 ALTER TABLE `t_parental_ratings` DISABLE KEYS */;
INSERT INTO `t_parental_ratings` VALUES (3,'Livre','Recomendado à todas as idades','T','2015-10-15 23:48:42',1,'2015-10-15 23:48:42',1),(4,'10','Não recomendado para menores de dez anos','T','2015-10-15 23:49:13',1,'2015-10-15 23:49:13',1),(5,'12','Não recomendado para menores de doze anos','T','2015-10-15 23:49:29',1,'2015-10-15 23:49:29',1),(6,'14','Não recomendado para menores de catorze anos','T','2015-10-15 23:49:59',1,'2015-10-15 23:49:59',1),(7,'16','Não recomendado para menores de dezesseis anos','T','2015-10-15 23:50:25',1,'2015-10-15 23:50:25',1),(8,'18','Não recomendado para menores de dezoito anos','T','2015-10-15 23:50:46',1,'2015-10-15 23:50:46',1);
/*!40000 ALTER TABLE `t_parental_ratings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_parental_ratings_archive`
--

DROP TABLE IF EXISTS `t_parental_ratings_archive`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_parental_ratings_archive` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `f_name` varchar(512) NOT NULL,
  `f_description` varchar(512) NOT NULL,
  `is_active` char(1) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `current_record` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by__idx` (`created_by`),
  KEY `modified_by__idx` (`modified_by`),
  KEY `current_record__idx` (`current_record`),
  CONSTRAINT `t_parental_ratings_archive_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `auth_user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `t_parental_ratings_archive_ibfk_2` FOREIGN KEY (`modified_by`) REFERENCES `auth_user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `t_parental_ratings_archive_ibfk_3` FOREIGN KEY (`current_record`) REFERENCES `t_parental_ratings` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_parental_ratings_archive`
--

LOCK TABLES `t_parental_ratings_archive` WRITE;
/*!40000 ALTER TABLE `t_parental_ratings_archive` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_parental_ratings_archive` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_pois`
--

DROP TABLE IF EXISTS `t_pois`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_pois` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `f_name` varchar(512) NOT NULL,
  `f_content` varchar(512) NOT NULL,
  `f_latitude` varchar(512) NOT NULL,
  `f_longitude` varchar(512) NOT NULL,
  `is_active` char(1) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `f_track` varchar(512) NOT NULL DEFAULT '0',
  `f_placing` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by__idx` (`created_by`),
  KEY `modified_by__idx` (`modified_by`),
  CONSTRAINT `t_pois_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `auth_user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `t_pois_ibfk_2` FOREIGN KEY (`modified_by`) REFERENCES `auth_user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_pois`
--

LOCK TABLES `t_pois` WRITE;
/*!40000 ALTER TABLE `t_pois` DISABLE KEYS */;
INSERT INTO `t_pois` VALUES (1,'Novo teste Escola Classe 316 sul teste','<p><a href=\"https://www.google.com.br/maps/place/Escola+Classe+316+Sul/@-15.8303625,-47.9279459,18z/data=!4m2!3m1!1s0x0000000000000000:0x38cd9a6e80fd612f\" target=\"_blank\">link </a>teste</p>\r\n\r\n<p><iframe align=\"middle\" frameborder=\"0\" height=\"315\" longdesc=\"Peace music\" name=\"Peace\" scrolling=\"no\" src=\"https://www.youtube.com/embed/qqTMKyAPSJA\" title=\"Peace\" width=\"420\"></iframe></p>','-15.8303625','-47.9279459','T','2015-10-16 00:02:36',1,'2016-03-08 17:34:02',1,'1',0),(2,'Igrejinha','<p><iframe frameborder=\"0\" scrolling=\"no\" src=\"https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/271591986&amp;amp;color=ff5500&amp;amp;auto_play=true&amp;amp;hide_related=false&amp;amp;show_comments=true&amp;amp;show_user=true&amp;amp;show_reposts=false\"></iframe></p>\r\n\r\n<p><iframe frameborder=\"0\" scrolling=\"no\" src=\"https://www.youtube.com/embed/AKGzRmMqqe8\"></iframe></p>','-15.814099','-47.903617','T','2015-10-16 00:06:37',1,'2016-07-01 10:28:40',1,'1',1),(3,'Hostpital Sarah Asa Sul','https://www.google.com.br/maps/place/Hospital+Sarah+Centro/@-15.7994099,-47.8905972,17.87z/data=!4m2!3m1!1s0x0000000000000000:0x7e5b696f056f059b','-15.7994099','-47.8905972','T','2015-10-16 00:10:29',1,'2015-10-21 15:34:10',1,'1',2),(4,'Parque da Cidade','https://www.google.com.br/maps/@-15.7933287,-47.8981324,90m/data=!3m1!1e3','-15.7933287','-47.8981324','T','2015-10-16 00:12:35',1,'2015-10-21 15:34:31',1,'1',3),(5,'ponto 1 azuleijos','conteudo ponto 1','-15.8303625','-47.903617','T','2015-10-16 17:57:28',1,'2015-10-21 15:34:40',1,'1',4),(6,'UnB','UnB - Concha Acústica','-15.7631573','-47.8706311','T','2015-10-21 15:21:10',1,'2015-10-21 15:34:58',1,'1',5),(7,'new one','teste','-15.7631573','-47.9279459','F','2015-10-21 16:06:27',1,'2015-10-21 16:06:27',1,'3',0),(8,'muléque','<p>teste</p>','-15.814099','-47.8706311','T','2015-10-21 16:08:13',1,'2015-12-08 17:47:25',1,'2',0),(9,'adasd','gfdg','-10','-10','T','2015-10-21 17:20:20',1,'2015-10-21 17:20:20',1,'7',1),(10,'hahah','hahah','-10','-10','T','2015-10-21 17:21:34',1,'2015-10-21 17:21:34',1,'2',1),(11,'lkasjlkasjld','laksjdlkajls','10','10','T','2015-10-21 17:23:37',1,'2015-10-21 17:23:37',1,'2',8),(12,'kjh','kjh','0','0','T','2015-10-21 17:25:41',1,'2015-10-21 17:25:41',1,'2',0),(13,'Kiko01','Kiko01','01','01','T','2015-10-21 17:33:32',1,'2015-10-21 17:33:32',1,'13',1),(14,'Kiko02','Kiko02','02','02','T','2015-10-21 17:33:52',1,'2015-10-21 17:33:52',1,'13',2),(15,'Kiko03','Kiko03','03','03','T','2015-10-21 17:37:45',1,'2015-10-21 17:37:45',1,'13',3),(16,'Kiko04','Kiko04','04','04','T','2015-10-21 17:38:02',1,'2015-10-21 17:38:02',1,'13',4),(17,'Kiko50','Kiko05','05','05','T','2015-10-21 17:38:34',1,'2015-10-21 17:38:34',1,'2',5),(18,'Kiko06','Kiko06','06','06','T','2015-10-21 17:40:39',1,'2015-10-21 17:40:39',1,'13',6),(19,'kiko07','kiko06','06','06','T','2015-10-21 17:45:26',1,'2015-10-21 17:45:26',1,'2',7),(20,'novo ponto teste','conteudo teste','-15.8303625','-47.903617','T','2015-10-28 15:56:45',1,'2015-10-28 15:56:45',1,'14',0),(21,'ponto teste 123','conteu teste 123','-15.8303625','-47.903617','T','2015-10-28 15:57:20',1,'2015-10-28 15:57:20',1,'14',1),(22,'uh','uh','01','01','T','2015-10-28 16:34:40',2,'2015-10-28 16:34:40',2,'15',0),(23,'uhu','uhu','02','02','T','2015-10-28 16:35:09',2,'2015-10-28 16:35:09',2,'15',1),(24,'Teste','teste','03','03','T','2015-10-28 16:35:46',2,'2015-10-28 16:35:46',2,'15',0),(25,'ponto sasdf','asdf lkaçsjdfoi jaw','-15.8303625','-47.903617','T','2015-10-28 16:37:43',1,'2015-10-28 16:37:43',1,'10',0),(26,'asdfcd','asfrfs sdf','-15.8303625','-47.903617','T','2015-10-28 16:39:24',1,'2015-10-28 16:39:24',1,'10',1),(27,'sada','dsfsdf','-15.8303625','-47.903617','T','2015-10-28 16:40:14',1,'2015-10-28 16:40:14',1,'15',2),(28,'uh','uh','04','04','T','2015-10-28 16:49:47',2,'2015-10-28 16:49:47',2,'15',0),(29,'uh2','uh2uh','05','05','T','2015-10-28 16:49:56',2,'2015-10-28 16:49:56',2,'15',1),(30,'uhuhu','huhuhu','1','1','T','2015-10-28 16:50:45',2,'2015-10-28 16:50:45',2,'15',2),(31,'uh','uh','12','12','T','2015-10-28 16:51:34',2,'2015-10-28 16:51:34',2,'15',3),(32,'uuhuh','uhuh','11','1','T','2015-10-28 16:54:12',2,'2015-10-28 16:54:12',2,'15',1),(33,'hhuhu','uhuh','1','1','T','2015-10-28 16:54:27',2,'2015-10-28 16:54:27',2,'10',1),(34,'huhuhuuhuhuhuhuh123','123','123','123','T','2015-10-28 16:54:56',2,'2015-10-28 16:54:56',2,'15',123),(35,'huhuhuuhuh1234','1234','1234','1234','T','2015-10-28 16:55:04',2,'2015-10-28 16:55:04',2,'15',124),(36,'Sei lá','http://http://www.lerolero.com/','-15','-14','T','2015-10-28 16:58:09',3,'2015-10-28 16:58:09',3,'15',2),(37,'Jurema BSB','http://http://www.lerolero.com/','-15','-14','T','2015-10-28 20:01:03',3,'2015-10-28 20:01:03',3,'16',0),(38,'Jurema BSB 02','http://http://www.lerolero.com/','-16','-14','T','2015-10-28 20:01:32',3,'2015-10-28 20:01:32',3,'16',1),(39,'teste de conteúdo com CKEditor','<p><strong>Teste do conte&uacute;do em html</strong></p>\r\n\r\n<p><img alt=\"Natural Trail\" src=\"http://img.americanas.com.br/produtos/01/00/item/120875/1/120875160_1GG.jpg\" style=\"float:right; height:500px; width:500px\" />lalala</p>\r\n\r\n<p>lelelele</p>\r\n\r\n<p>lililililili</p>\r\n\r\n<p>hohohohoh</p>','-15.684526347832596','-47.826136350631714','T','2015-11-17 15:56:01',1,'2015-11-17 15:56:01',1,'20',0),(40,'Ponto da trilha de teste???????','<h1><em><strong>Gostei do editor de texto</strong></em> <img alt=\"yes\" src=\"https://late.art.br:8000/Trilhas02/static/plugin_ckeditor/plugins/smiley/images/thumbs_up.png\" style=\"height:23px; width:23px\" title=\"yes\" /></h1>\r\n\r\n<p>&nbsp;</p>','-15.012031248214925','-46.99500560760498','T','2015-11-18 00:40:50',3,'2015-11-25 13:55:05',3,'21',0),(41,'Testando a trilha','<p>Descri&ccedil;&atilde;o do ponto?</p>','-15.00714277168632','-47.03616142272949','F','2015-11-18 00:41:25',3,'2015-11-18 00:41:25',3,'21',1),(42,'Dança','<p>Farfalham as persianas</p>\r\n\r\n<p>num bailado aleat&oacute;rio</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Uma fuma&ccedil;a sobe</p>\r\n\r\n<p>do caf&eacute; que vai embora</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Um sopro de fora guia a dan&ccedil;a</p>\r\n\r\n<p>trazendo um sussurro</p>\r\n\r\n<p>do planalto dos ventos uivantes</p>','-15.772026027008858','-47.87560164928436','T','2015-11-20 17:43:01',4,'2016-07-04 00:43:06',4,'24',0),(43,'Benedictus','<p>Nada me paga</p>\r\n\r\n<p>o sol que aquece minha pele aos poucos,</p>\r\n\r\n<p>as pessoas que encontro pelo caminho,</p>\r\n\r\n<p>o sorriso sincero de amigos,</p>\r\n\r\n<p>os carros que passam na rua sem parar</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Num papel descrevo</p>\r\n\r\n<p>a festa que se celebra em cada canto,</p>\r\n\r\n<p>a vida que floresce nas cal&ccedil;adas,</p>\r\n\r\n<p>o novo dia que j&aacute; come&ccedil;ou</p>','-15.769894955664025','-47.87542516045505','T','2015-11-20 17:44:43',4,'2016-07-04 00:58:11',4,'24',1),(44,'teste readOnly','<p>teste readOnly</p>','-15.693863862567484','-47.82868981361389','F','2015-11-25 02:17:15',1,'2015-11-25 02:17:15',1,'1',6),(45,'Teste de edição','<p>lalalalalalallalalallalla</p>','-14.999349718620113','-47.006592750549316','T','2015-11-25 13:51:42',3,'2015-11-25 13:51:42',3,'17',1),(46,'teste 2015','<p>lalalalalals</p>','-15.018748922205882','-46.940674781799316','T','2015-11-25 13:52:51',3,'2015-11-25 13:55:56',3,'26',0),(47,'teste 002','<p>lalalalala 02</p>','-15.01079048613608','-46.9755220413208','T','2015-11-25 13:53:13',3,'2015-11-25 13:53:13',3,'26',1),(48,'Primeiro Ponto!','<p><strong>Ol&aacute;!</strong></p>\r\n\r\n<p>Tudo come&ccedil;a na feira do paraguai!!</p>','-15.795846246643386','-47.94935703277588','F','2015-11-25 16:14:31',2,'2015-11-25 16:41:38',2,'27',0),(49,'Ponto 02','<p>Depois&nbsp;vamos ao parque da cidade</p>','-15.795392003736167','-47.90433883666992','T','2015-11-25 16:14:56',2,'2015-11-25 16:14:56',2,'27',1),(50,'Nadar','<p>Nadar no lago...</p>','-15.780360118526076','-47.83721923828125','T','2015-11-25 16:15:11',2,'2015-11-25 16:15:11',2,'27',2),(51,'Jogar bola','<p>bater uma bolinha!</p>','-15.783870411283246','-47.89897441864014','T','2015-11-25 16:15:40',2,'2015-11-25 16:15:40',2,'27',3),(52,'D2','<p>Dar um dois...</p>','-15.8018338988214','-47.812156677246094','T','2015-11-25 16:16:03',2,'2015-11-25 16:16:03',2,'27',4),(53,'Banho na cachu','<p>Pra tirar a mazela, l&aacute; no urubu..</p>','-15.710926446282558','-47.86160588264465','T','2015-11-25 16:18:22',2,'2015-11-25 16:18:22',2,'27',5),(54,'Rango irado','<p>La no Quituart, no Jamburanas!</p>','-15.74308532281495','-47.85191774368286','T','2015-11-25 16:19:24',2,'2015-11-25 16:19:24',2,'27',6),(55,'127.0.0.1','<p>Home sweet home!</p>','-15.79440092476615','-47.93978691101074','T','2015-11-25 16:19:49',2,'2015-11-25 16:19:49',2,'27',7),(56,'sdsdss','<p>asadasdasdasd</p>','-15.795887541402623','-47.87722706794739','T','2015-12-04 18:10:05',5,'2015-12-04 18:10:05',5,'30',0),(57,'SMU','<p>tesssste 123 321 testando</p>','-15.76540972068106','-47.92188048362732','T','2016-03-22 20:21:53',1,'2016-03-22 20:21:53',1,'31',0),(58,'Ponto G','<p>PONTO G Content</p>\r\n\r\n<p>Graficos</p>\r\n\r\n<p>&nbsp;</p>','-15.793575021920164','-47.88266658782959','T','2016-03-22 22:03:45',1,'2016-03-22 22:03:45',1,'32',0),(59,'Ponto H','<p>Ponto H</p>','-15.808771095070847','-48.08123588562012','T','2016-03-22 22:04:44',1,'2016-03-22 22:04:44',1,'32',1),(60,'Umbigbang','<p><img alt=\"\" src=\"https://fbcdn-sphotos-d-a.akamaihd.net/hphotos-ak-xfp1/v/t1.0-9/13096094_1748894461992787_3768272401273522217_n.png?oh=95848ad9e97727bd162a2b1b457c4421&amp;oe=57A88E55&amp;__gda__=1470584581_01127ae7cf03d62055174ca60e9aa0da\" style=\"height:448px; width:680px\" /></p>','12.438721712448265','42.0611572265625','T','2016-05-08 23:33:19',4,'2016-05-08 23:33:19',4,'41',0),(61,'Origem','<p>&lt;iframe width=&quot;420&quot; height=&quot;315&quot; src=&quot;https://www.youtube.com/embed/AHoKXTb66Ro&quot; frameborder=&quot;0&quot; allowfullscreen&gt;&lt;/iframe&gt;</p>','-0.052734903996226365','0.0054931640625','T','2016-05-08 23:40:21',4,'2016-05-08 23:40:21',4,'41',1),(62,'lala','<p>asdfasdf</p>','-15.799356271118059','-47.952919006347656','T','2016-05-11 17:28:41',1,'2016-05-11 17:28:41',1,'44',0),(63,'asdasd','<p>asdasd</p>','-15.795763657099657','-47.94705033302307','T','2016-05-11 17:33:44',2,'2016-05-11 17:33:44',2,'45',0),(64,'asdasd','<p>asdasd</p>','-15.798943330219947','-47.968422174453735','T','2016-05-11 18:03:07',2,'2016-05-11 18:03:07',2,'1',6),(65,'abcD','<p>acvs</p>','-15.794896464857334','-47.94738829135895','T','2016-07-03 10:32:42',2,'2016-07-03 10:32:42',2,'48',0),(66,'asdasd','<p>asda</p>','-15.002038631725206','-47.00707018375397','T','2016-07-03 10:33:15',2,'2016-07-03 10:33:15',2,'48',1),(67,'ponto 3','<p>a designar</p>','-15.767943519213372','-47.874253035115544','T','2016-07-04 21:13:26',4,'2016-07-04 21:13:26',4,'24',2);
/*!40000 ALTER TABLE `t_pois` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_pois_archive`
--

DROP TABLE IF EXISTS `t_pois_archive`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_pois_archive` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `f_name` varchar(512) NOT NULL,
  `f_content` varchar(512) NOT NULL,
  `f_latitude` varchar(512) NOT NULL,
  `f_longitude` varchar(512) NOT NULL,
  `is_active` char(1) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `current_record` int(11) DEFAULT NULL,
  `f_track` varchar(512) NOT NULL DEFAULT '0',
  `f_placing` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by__idx` (`created_by`),
  KEY `modified_by__idx` (`modified_by`),
  KEY `current_record__idx` (`current_record`),
  CONSTRAINT `t_pois_archive_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `auth_user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `t_pois_archive_ibfk_2` FOREIGN KEY (`modified_by`) REFERENCES `auth_user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `t_pois_archive_ibfk_3` FOREIGN KEY (`current_record`) REFERENCES `t_pois` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_pois_archive`
--

LOCK TABLES `t_pois_archive` WRITE;
/*!40000 ALTER TABLE `t_pois_archive` DISABLE KEYS */;
INSERT INTO `t_pois_archive` VALUES (1,'Escola Classe 316 sul','https://www.google.com.br/maps/place/Escola+Classe+316+Sul/@-15.8303625,-47.9279459,18z/data=!4m2!3m1!1s0x0000000000000000:0x38cd9a6e80fd612f','-15.8303625','-47.9279459','T','2015-10-16 00:02:36',1,'2015-10-16 00:02:36',1,1,'0',0),(2,'Igrejinha','https://www.google.com.br/maps/@-15.8140512,-47.9035948,75m/data=!3m1!1e3','-15.814099','-47.903617','T','2015-10-16 00:06:37',1,'2015-10-16 00:06:37',1,2,'0',0),(3,'Hostpital Sarah Asa Sul','https://www.google.com.br/maps/place/Hospital+Sarah+Centro/@-15.7994099,-47.8905972,17.87z/data=!4m2!3m1!1s0x0000000000000000:0x7e5b696f056f059b','-15.7994099','-47.8905972','T','2015-10-16 00:10:29',1,'2015-10-16 00:10:29',1,3,'0',0),(4,'Parque da Cidade','https://www.google.com.br/maps/@-15.7933287,-47.8981324,90m/data=!3m1!1e3','-15.7933287','-47.8981324','T','2015-10-16 00:12:35',1,'2015-10-16 00:12:35',1,4,'0',0),(5,'Igrejinha','https://www.google.com.br/maps/@-15.8140512,-47.9035948,75m/data=!3m1!1e3','-15.814099','-47.903617','T','2015-10-16 00:06:37',1,'2015-10-21 15:23:10',1,2,'1',0),(6,'Hostpital Sarah Asa Sul','https://www.google.com.br/maps/place/Hospital+Sarah+Centro/@-15.7994099,-47.8905972,17.87z/data=!4m2!3m1!1s0x0000000000000000:0x7e5b696f056f059b','-15.7994099','-47.8905972','T','2015-10-16 00:10:29',1,'2015-10-21 15:23:26',1,3,'1',0),(7,'Parque da Cidade','https://www.google.com.br/maps/@-15.7933287,-47.8981324,90m/data=!3m1!1e3','-15.7933287','-47.8981324','T','2015-10-16 00:12:35',1,'2015-10-21 15:23:40',1,4,'1',0),(8,'ponto 1 azuleijos','conteudo ponto 1','-15.8303625','-47.903617','T','2015-10-16 17:57:28',1,'2015-10-16 17:57:28',1,5,'1',0),(9,'UnB','UnB - Concha Acústica','-15.7631573','-47.8706311','T','2015-10-21 15:21:10',1,'2015-10-21 15:21:10',1,6,'1',0);
/*!40000 ALTER TABLE `t_pois_archive` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_tags`
--

DROP TABLE IF EXISTS `t_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `f_name` varchar(512) NOT NULL,
  `is_active` char(1) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by__idx` (`created_by`),
  KEY `modified_by__idx` (`modified_by`),
  CONSTRAINT `t_tags_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `auth_user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `t_tags_ibfk_2` FOREIGN KEY (`modified_by`) REFERENCES `auth_user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_tags`
--

LOCK TABLES `t_tags` WRITE;
/*!40000 ALTER TABLE `t_tags` DISABLE KEYS */;
INSERT INTO `t_tags` VALUES (2,'tagTeste1','T','2016-05-03 17:44:33',1,'2016-05-03 17:44:33',1),(3,'tagTeste2','T','2016-05-03 17:44:44',1,'2016-05-03 17:44:44',1),(10,'456','T','2016-05-05 01:30:24',1,'2016-05-05 02:50:09',1),(11,'789','T','2016-05-05 01:31:17',1,'2016-05-05 01:31:17',1),(12,'123','T','2016-05-05 01:33:27',1,'2016-05-12 21:43:09',1),(13,'uppercase','T','2016-05-05 01:36:28',1,'2016-05-05 02:50:09',1),(14,'312','T','2016-05-05 02:50:09',1,'2016-05-05 02:50:09',1),(15,'teste','T','2016-05-11 16:40:16',2,'2016-05-11 16:40:16',2),(16,'teste2','T','2016-05-11 16:40:33',2,'2016-05-11 16:40:33',2),(17,'abc','T','2016-05-11 17:01:57',2,'2016-05-11 17:01:57',2),(18,'asd','T','2016-05-11 17:11:41',2,'2016-05-11 17:33:27',2),(19,'asdfd','T','2016-05-12 21:43:09',1,'2016-05-12 21:43:09',1),(20,'fasdf','T','2016-05-12 21:43:09',1,'2016-05-12 21:43:09',1),(21,'asdf','T','2016-05-12 21:43:09',1,'2016-05-12 21:43:09',1),(22,'urubu','T','2016-05-19 16:53:19',6,'2016-05-19 16:53:19',6),(23,'as ','T','2016-07-03 10:32:18',2,'2016-07-03 10:32:18',2),(24,'poesia','T','2016-07-04 00:22:01',4,'2016-07-04 00:22:01',4),(25,' poemas','T','2016-07-04 00:22:01',4,'2016-07-04 00:22:01',4),(26,' coisas pequeninas','T','2016-07-04 00:22:01',4,'2016-07-04 00:22:01',4),(27,' unb','T','2016-07-04 00:22:01',4,'2016-07-04 00:22:01',4);
/*!40000 ALTER TABLE `t_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_tags_archive`
--

DROP TABLE IF EXISTS `t_tags_archive`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_tags_archive` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `f_name` varchar(512) NOT NULL,
  `is_active` char(1) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `current_record` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by__idx` (`created_by`),
  KEY `modified_by__idx` (`modified_by`),
  KEY `current_record__idx` (`current_record`),
  CONSTRAINT `t_tags_archive_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `auth_user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `t_tags_archive_ibfk_2` FOREIGN KEY (`modified_by`) REFERENCES `auth_user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `t_tags_archive_ibfk_3` FOREIGN KEY (`current_record`) REFERENCES `t_tags` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_tags_archive`
--

LOCK TABLES `t_tags_archive` WRITE;
/*!40000 ALTER TABLE `t_tags_archive` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_tags_archive` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_track_pois`
--

DROP TABLE IF EXISTS `t_track_pois`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_track_pois` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `f_track` int(11) NOT NULL,
  `f_poi` int(11) NOT NULL,
  `f_order` int(11) DEFAULT NULL,
  `is_active` char(1) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `f_track__idx` (`f_track`),
  KEY `f_poi__idx` (`f_poi`),
  KEY `created_by__idx` (`created_by`),
  KEY `modified_by__idx` (`modified_by`),
  CONSTRAINT `t_track_pois_ibfk_1` FOREIGN KEY (`f_track`) REFERENCES `t_tracks` (`id`) ON DELETE CASCADE,
  CONSTRAINT `t_track_pois_ibfk_2` FOREIGN KEY (`f_poi`) REFERENCES `t_pois` (`id`) ON DELETE CASCADE,
  CONSTRAINT `t_track_pois_ibfk_3` FOREIGN KEY (`created_by`) REFERENCES `auth_user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `t_track_pois_ibfk_4` FOREIGN KEY (`modified_by`) REFERENCES `auth_user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_track_pois`
--

LOCK TABLES `t_track_pois` WRITE;
/*!40000 ALTER TABLE `t_track_pois` DISABLE KEYS */;
INSERT INTO `t_track_pois` VALUES (1,1,1,1,'T','2015-10-16 00:13:12',1,'2015-10-16 00:13:12',1),(2,1,2,2,'T','2015-10-16 00:13:25',1,'2015-10-16 00:13:25',1),(3,1,3,3,'T','2015-10-16 00:13:57',1,'2015-10-16 00:13:57',1),(4,1,4,4,'T','2015-10-16 00:14:07',1,'2015-10-16 00:14:07',1);
/*!40000 ALTER TABLE `t_track_pois` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_track_pois_archive`
--

DROP TABLE IF EXISTS `t_track_pois_archive`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_track_pois_archive` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `f_track` int(11) NOT NULL,
  `f_poi` int(11) NOT NULL,
  `f_order` int(11) DEFAULT NULL,
  `is_active` char(1) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `current_record` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `f_track__idx` (`f_track`),
  KEY `f_poi__idx` (`f_poi`),
  KEY `created_by__idx` (`created_by`),
  KEY `modified_by__idx` (`modified_by`),
  KEY `current_record__idx` (`current_record`),
  CONSTRAINT `t_track_pois_archive_ibfk_1` FOREIGN KEY (`f_track`) REFERENCES `t_tracks` (`id`) ON DELETE CASCADE,
  CONSTRAINT `t_track_pois_archive_ibfk_2` FOREIGN KEY (`f_poi`) REFERENCES `t_pois` (`id`) ON DELETE CASCADE,
  CONSTRAINT `t_track_pois_archive_ibfk_3` FOREIGN KEY (`created_by`) REFERENCES `auth_user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `t_track_pois_archive_ibfk_4` FOREIGN KEY (`modified_by`) REFERENCES `auth_user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `t_track_pois_archive_ibfk_5` FOREIGN KEY (`current_record`) REFERENCES `t_track_pois` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_track_pois_archive`
--

LOCK TABLES `t_track_pois_archive` WRITE;
/*!40000 ALTER TABLE `t_track_pois_archive` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_track_pois_archive` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_track_ratings`
--

DROP TABLE IF EXISTS `t_track_ratings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_track_ratings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `f_score` varchar(512) NOT NULL,
  `f_track` int(11) NOT NULL,
  `f_date` datetime NOT NULL,
  `is_active` char(1) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `f_feedback` varchar(512) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `f_track__idx` (`f_track`),
  KEY `created_by__idx` (`created_by`),
  KEY `modified_by__idx` (`modified_by`),
  CONSTRAINT `t_track_ratings_ibfk_1` FOREIGN KEY (`f_track`) REFERENCES `t_tracks` (`id`) ON DELETE CASCADE,
  CONSTRAINT `t_track_ratings_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `auth_user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `t_track_ratings_ibfk_3` FOREIGN KEY (`modified_by`) REFERENCES `auth_user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_track_ratings`
--

LOCK TABLES `t_track_ratings` WRITE;
/*!40000 ALTER TABLE `t_track_ratings` DISABLE KEYS */;
INSERT INTO `t_track_ratings` VALUES (1,'3',1,'2015-10-16 00:14:36','T','2015-10-16 00:14:40',1,'2015-10-16 00:14:40',1,''),(2,'1',28,'2016-07-02 16:01:18','T','2016-07-02 16:00:50',1,'2016-07-02 16:00:50',1,'feedback teste');
/*!40000 ALTER TABLE `t_track_ratings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_track_ratings_archive`
--

DROP TABLE IF EXISTS `t_track_ratings_archive`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_track_ratings_archive` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `f_score` varchar(512) NOT NULL,
  `f_track` int(11) NOT NULL,
  `f_date` datetime NOT NULL,
  `is_active` char(1) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `current_record` int(11) DEFAULT NULL,
  `f_feedback` varchar(512) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `f_track__idx` (`f_track`),
  KEY `created_by__idx` (`created_by`),
  KEY `modified_by__idx` (`modified_by`),
  KEY `current_record__idx` (`current_record`),
  CONSTRAINT `t_track_ratings_archive_ibfk_1` FOREIGN KEY (`f_track`) REFERENCES `t_tracks` (`id`) ON DELETE CASCADE,
  CONSTRAINT `t_track_ratings_archive_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `auth_user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `t_track_ratings_archive_ibfk_3` FOREIGN KEY (`modified_by`) REFERENCES `auth_user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `t_track_ratings_archive_ibfk_4` FOREIGN KEY (`current_record`) REFERENCES `t_track_ratings` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_track_ratings_archive`
--

LOCK TABLES `t_track_ratings_archive` WRITE;
/*!40000 ALTER TABLE `t_track_ratings_archive` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_track_ratings_archive` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_track_tags`
--

DROP TABLE IF EXISTS `t_track_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_track_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `f_track` int(11) NOT NULL,
  `f_tag` int(11) NOT NULL,
  `is_active` char(1) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `f_track__idx` (`f_track`),
  KEY `f_tag__idx` (`f_tag`),
  KEY `created_by__idx` (`created_by`),
  KEY `modified_by__idx` (`modified_by`),
  CONSTRAINT `t_track_tags_ibfk_1` FOREIGN KEY (`f_track`) REFERENCES `t_tracks` (`id`) ON DELETE CASCADE,
  CONSTRAINT `t_track_tags_ibfk_2` FOREIGN KEY (`f_tag`) REFERENCES `t_tags` (`id`) ON DELETE CASCADE,
  CONSTRAINT `t_track_tags_ibfk_3` FOREIGN KEY (`created_by`) REFERENCES `auth_user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `t_track_tags_ibfk_4` FOREIGN KEY (`modified_by`) REFERENCES `auth_user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_track_tags`
--

LOCK TABLES `t_track_tags` WRITE;
/*!40000 ALTER TABLE `t_track_tags` DISABLE KEYS */;
INSERT INTO `t_track_tags` VALUES (1,31,2,'T','2016-05-03 17:45:19',1,'2016-05-03 17:45:19',1),(2,31,3,'T','2016-05-03 17:45:36',1,'2016-05-03 17:45:36',1),(3,31,2,'T','2016-05-03 17:45:44',1,'2016-05-03 17:45:44',1),(5,37,10,'T','2016-05-05 01:30:24',1,'2016-05-05 01:30:24',1),(6,38,2,'T','2016-05-05 01:31:17',1,'2016-05-05 01:31:17',1),(7,38,11,'T','2016-05-05 01:31:17',1,'2016-05-05 01:31:17',1),(8,39,2,'T','2016-05-05 01:33:27',1,'2016-05-05 01:33:27',1),(9,39,12,'T','2016-05-05 01:33:27',1,'2016-05-05 01:33:27',1),(12,40,10,'T','2016-05-05 02:50:09',1,'2016-05-05 02:50:09',1),(13,40,13,'T','2016-05-05 02:50:09',1,'2016-05-05 02:50:09',1),(14,40,12,'T','2016-05-05 02:50:09',1,'2016-05-05 02:50:09',1),(15,40,14,'T','2016-05-05 02:50:09',1,'2016-05-05 02:50:09',1),(19,1,12,'T','2016-05-11 16:40:33',2,'2016-05-11 16:40:33',2),(20,1,16,'T','2016-05-11 16:40:33',2,'2016-05-11 16:40:33',2),(21,42,17,'T','2016-05-11 17:01:57',2,'2016-05-11 17:01:57',2),(22,43,18,'T','2016-05-11 17:11:41',2,'2016-05-11 17:11:41',2),(23,45,18,'T','2016-05-11 17:33:27',2,'2016-05-11 17:33:27',2),(24,46,12,'T','2016-05-12 21:43:09',1,'2016-05-12 21:43:09',1),(25,46,19,'T','2016-05-12 21:43:09',1,'2016-05-12 21:43:09',1),(26,46,20,'T','2016-05-12 21:43:09',1,'2016-05-12 21:43:09',1),(27,46,21,'T','2016-05-12 21:43:09',1,'2016-05-12 21:43:09',1),(28,47,22,'T','2016-05-19 16:53:19',6,'2016-05-19 16:53:19',6),(29,48,23,'T','2016-07-03 10:32:18',2,'2016-07-03 10:32:18',2),(30,24,24,'T','2016-07-04 00:22:01',4,'2016-07-04 00:22:01',4),(31,24,25,'T','2016-07-04 00:22:01',4,'2016-07-04 00:22:01',4),(32,24,26,'T','2016-07-04 00:22:01',4,'2016-07-04 00:22:01',4),(33,24,27,'T','2016-07-04 00:22:01',4,'2016-07-04 00:22:01',4);
/*!40000 ALTER TABLE `t_track_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_track_tags_archive`
--

DROP TABLE IF EXISTS `t_track_tags_archive`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_track_tags_archive` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `f_track` int(11) NOT NULL,
  `f_tag` int(11) NOT NULL,
  `is_active` char(1) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `current_record` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `f_track__idx` (`f_track`),
  KEY `f_tag__idx` (`f_tag`),
  KEY `created_by__idx` (`created_by`),
  KEY `modified_by__idx` (`modified_by`),
  KEY `current_record__idx` (`current_record`),
  CONSTRAINT `t_track_tags_archive_ibfk_1` FOREIGN KEY (`f_track`) REFERENCES `t_tracks` (`id`) ON DELETE CASCADE,
  CONSTRAINT `t_track_tags_archive_ibfk_2` FOREIGN KEY (`f_tag`) REFERENCES `t_tags` (`id`) ON DELETE CASCADE,
  CONSTRAINT `t_track_tags_archive_ibfk_3` FOREIGN KEY (`created_by`) REFERENCES `auth_user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `t_track_tags_archive_ibfk_4` FOREIGN KEY (`modified_by`) REFERENCES `auth_user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `t_track_tags_archive_ibfk_5` FOREIGN KEY (`current_record`) REFERENCES `t_track_tags` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_track_tags_archive`
--

LOCK TABLES `t_track_tags_archive` WRITE;
/*!40000 ALTER TABLE `t_track_tags_archive` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_track_tags_archive` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_tracks`
--

DROP TABLE IF EXISTS `t_tracks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_tracks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `f_name` varchar(512) NOT NULL,
  `f_parental_rating` int(11) NOT NULL,
  `f_description` varchar(512) NOT NULL,
  `f_owner` int(11) NOT NULL,
  `f_create_date` datetime NOT NULL,
  `is_active` char(1) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `f_icon` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `f_parental_rating__idx` (`f_parental_rating`),
  KEY `f_owner__idx` (`f_owner`),
  KEY `created_by__idx` (`created_by`),
  KEY `modified_by__idx` (`modified_by`),
  CONSTRAINT `t_tracks_ibfk_1` FOREIGN KEY (`f_parental_rating`) REFERENCES `t_parental_ratings` (`id`) ON DELETE CASCADE,
  CONSTRAINT `t_tracks_ibfk_2` FOREIGN KEY (`f_owner`) REFERENCES `auth_user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `t_tracks_ibfk_3` FOREIGN KEY (`created_by`) REFERENCES `auth_user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `t_tracks_ibfk_4` FOREIGN KEY (`modified_by`) REFERENCES `auth_user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_tracks`
--

LOCK TABLES `t_tracks` WRITE;
/*!40000 ALTER TABLE `t_tracks` DISABLE KEYS */;
INSERT INTO `t_tracks` VALUES (1,'Trilha dos Azuleijos1',3,'321 Um passeio por Brasília pelas obras de Athos Bulcão321',1,'2015-10-15 23:55:06','T','2015-10-15 23:55:15',1,'2016-05-11 16:40:33',2,NULL),(2,'Trilha dos muleques',3,'Descrição dos muleques',1,'2015-10-16 17:27:52','T','2015-10-16 17:28:14',1,'2015-10-16 17:28:14',1,NULL),(3,'nova track',5,'nova track',1,'2015-10-16 17:58:59','T','2015-10-16 17:59:17',1,'2015-10-16 17:59:17',1,NULL),(7,'sadas',5,'asdfasdf',1,'2015-10-16 18:03:30','F','2015-10-16 18:04:42',1,'2015-11-11 09:56:05',2,NULL),(17,'Jurema em Brasilia',3,'Jurema trilha um caminho em Brasilia',3,'2015-11-06 17:28:40','F','2015-11-06 17:29:03',3,'2015-11-06 17:29:03',3,NULL),(19,'Trilha das Jaqueiras',3,'Pé na Jaca',1,'2015-11-11 16:34:45','T','2015-11-11 16:34:45',1,'2015-11-11 16:34:45',1,NULL),(20,'Teste Cadastro',3,'Teste do cadastro de uma nova trilha poética, o sistema não permitiu a inserção sem todos os campos estarem preenchidos.',1,'2015-11-17 12:15:16','T','2015-11-17 12:15:16',1,'2015-11-17 12:15:16',1,NULL),(21,'Trilha de Teste',7,'Trilha de teste e la la la la la la la',3,'2015-11-18 00:39:06','T','2015-11-18 00:39:06',3,'2015-11-18 00:39:06',3,NULL),(22,'tese screenShot',3,'tese screenShot',1,'2015-11-18 17:27:02','T','2015-11-18 17:27:02',1,'2015-11-18 17:27:02',1,NULL),(23,'Trilha ratsHole',3,'Descrição da rathole',1,'2015-11-20 00:11:54','T','2015-11-20 00:11:54',1,'2015-11-20 00:11:54',1,NULL),(24,'Trilha das coisas pequeninas',5,'Um passeio pela bucólica Asa Norte com as lentes que desvelam as coisas pequeninas do dia-a-dia',4,'2015-11-20 17:40:51','T','2015-11-20 17:40:51',4,'2016-07-04 00:22:01',4,'t_tracks.f_icon.8a35a561f7a6b50a.64656661756c745f69636f6e5f747261636b2e706e67.png'),(25,'teste 22',3,'#',4,'2015-11-20 18:02:00','T','2015-11-20 18:02:00',4,'2015-11-20 18:02:00',4,NULL),(26,'Ponto da trilha de teste',3,'nao sei',3,'2015-11-25 13:52:22','T','2015-11-25 13:52:22',3,'2015-11-25 13:52:22',3,NULL),(27,'Trilha Final',7,'Uma Trilha Para Testar Todas As Funções',2,'2015-11-25 16:10:50','F','2015-11-25 16:10:50',2,'2015-11-25 16:10:50',2,NULL),(28,'Trilha dos Azuleijos1',3,'123 Um passeio por Brasília pelas obras de Athos Bulcão',1,'2015-11-26 16:14:40','T','2015-11-26 16:14:40',1,'2015-11-26 16:14:40',1,NULL),(29,'mais um teste',3,'mais um teste',1,'2015-11-27 15:47:35','T','2015-11-27 15:47:35',1,'2015-11-27 15:47:35',1,NULL),(30,'Teste 01',5,'teste maluko',5,'2015-12-04 17:26:53','T','2015-12-04 17:26:53',5,'2015-12-04 17:26:53',5,NULL),(31,'Casa do Paulo',3,'Casa do Pauli',1,'2016-03-22 20:21:02','T','2016-03-22 20:21:02',1,'2016-03-22 20:21:02',1,NULL),(32,'Palavra',8,'Sentido que tentamos alcançar',1,'2016-03-22 22:02:07','T','2016-03-22 22:02:07',1,'2016-03-22 22:02:07',1,NULL),(33,'Trilha mobile',3,'Sentido que tentamos alcançar',1,'2016-04-09 22:41:48','T','2016-04-09 22:41:48',1,'2016-04-09 22:41:48',1,NULL),(34,'teste2',3,'asdf',1,'2016-05-03 15:31:14','T','2016-05-03 15:31:14',1,'2016-05-03 15:31:14',1,NULL),(35,'teste tag',3,'teste tag',1,'2016-05-03 17:14:16','T','2016-05-03 17:14:16',1,'2016-05-03 17:14:16',1,NULL),(36,'teste1233222',4,'asdfads',1,'2016-05-04 23:40:25','T','2016-05-04 23:40:25',1,'2016-05-04 23:40:25',1,NULL),(37,'teste1233222',4,'asdfads',1,'2016-05-05 01:30:24','T','2016-05-05 01:30:24',1,'2016-05-05 02:46:14',1,NULL),(38,'teste persist tag 2',3,'teste persist tag 2',1,'2016-05-05 01:31:17','T','2016-05-05 01:31:17',1,'2016-05-05 01:31:17',1,NULL),(39,'teste persist tag 3',3,'teste persist tag 3',1,'2016-05-05 01:33:27','T','2016-05-05 01:33:27',1,'2016-05-05 01:33:27',1,NULL),(40,'teste persist tag 4',3,'teste persist tag 4',1,'2016-05-05 01:36:28','T','2016-05-05 01:36:28',1,'2016-05-05 02:50:09',1,NULL),(41,'Umbigbang',3,'1 big bang, 2 big bang, ...',4,'2016-05-08 02:21:38','T','2016-05-08 02:21:38',4,'2016-05-08 02:21:38',4,NULL),(42,'ABC',5,'teste',2,'2016-05-11 17:01:57','T','2016-05-11 17:01:57',2,'2016-05-11 17:01:57',2,'t_tracks.f_icon.b4f700f7de54a7ef.53637265656e2053686f7420323031362d30332d30332061742031312e35352e31362e706e67.png'),(43,'ABCD',4,'ABCD',2,'2016-05-11 17:11:41','T','2016-05-11 17:11:41',2,'2016-05-11 17:11:41',2,'t_tracks.f_icon.8f3dd8e907d3b481.64656661756c745f69636f6e5f747261636b2e706e67.png'),(44,'teste nova trilha botão editar',3,'teste nova trilha botão editar',1,'2016-05-11 17:28:11','T','2016-05-11 17:28:11',1,'2016-05-11 17:28:11',1,'t_tracks.f_icon.80cd1e673b9f7205.64656661756c745f69636f6e5f747261636b2e706e67.png'),(45,'asdasd',5,'asdasd',2,'2016-05-11 17:33:27','T','2016-05-11 17:33:27',2,'2016-05-11 17:33:27',2,'t_tracks.f_icon.8ac9ce86d7ae35c7.64656661756c745f69636f6e5f747261636b2e706e67.png'),(46,'asdfasdf',4,'fffff',1,'2016-05-12 21:43:09','T','2016-05-12 21:43:09',1,'2016-05-12 21:43:09',1,'t_tracks.f_icon.97d1bedb24290888.494d475f363633312e4a5047.JPG'),(47,'trilha do urubu',3,'venha',6,'2016-05-19 16:53:19','T','2016-05-19 16:53:19',6,'2016-05-19 16:53:19',6,'t_tracks.f_icon.a5e46b9292dc1d4e.64656661756c745f69636f6e5f747261636b2e706e67.png'),(48,'sad',4,'asdasd',2,'2016-07-03 10:32:18','T','2016-07-03 10:32:18',2,'2016-07-03 10:32:18',2,'t_tracks.f_icon.ae5858d8babd1347.64656661756c745f69636f6e5f747261636b2e706e67.png');
/*!40000 ALTER TABLE `t_tracks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_tracks_archive`
--

DROP TABLE IF EXISTS `t_tracks_archive`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_tracks_archive` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `f_name` varchar(512) NOT NULL,
  `f_parental_rating` int(11) NOT NULL,
  `f_description` varchar(512) NOT NULL,
  `f_owner` int(11) NOT NULL,
  `f_create_date` datetime NOT NULL,
  `is_active` char(1) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `current_record` int(11) DEFAULT NULL,
  `f_icon` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `f_parental_rating__idx` (`f_parental_rating`),
  KEY `f_owner__idx` (`f_owner`),
  KEY `created_by__idx` (`created_by`),
  KEY `modified_by__idx` (`modified_by`),
  KEY `current_record__idx` (`current_record`),
  CONSTRAINT `t_tracks_archive_ibfk_1` FOREIGN KEY (`f_parental_rating`) REFERENCES `t_parental_ratings` (`id`) ON DELETE CASCADE,
  CONSTRAINT `t_tracks_archive_ibfk_2` FOREIGN KEY (`f_owner`) REFERENCES `auth_user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `t_tracks_archive_ibfk_3` FOREIGN KEY (`created_by`) REFERENCES `auth_user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `t_tracks_archive_ibfk_4` FOREIGN KEY (`modified_by`) REFERENCES `auth_user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `t_tracks_archive_ibfk_5` FOREIGN KEY (`current_record`) REFERENCES `t_tracks` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_tracks_archive`
--

LOCK TABLES `t_tracks_archive` WRITE;
/*!40000 ALTER TABLE `t_tracks_archive` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_tracks_archive` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-07-06 20:38:20
